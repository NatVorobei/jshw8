//1. DOM це об'єктна модель документа, будує дерево з HTML-коду
//2. innerHTML - вміст вузла як HTML, innerText - вміст вузла як текст
//3. Через getElement*, querySelector*. Кращий спосіб дивлячись для якої задачі, querySelector трохи повільніший, 
// ніж getElementById наприклад, але може обробляти не тільки id, а й класи, атрибути тощо.

let paragraph = document.querySelectorAll("p");
for(const p of paragraph){
    p.style.backgroundColor = '#ff0000';
}

let opList = document.getElementById('optionsList');
console.log(opList);
console.log(opList.parentElement);

opList = document.getElementById('optionsList').childNodes;
for(let node of opList){
    console.log(`Тип ноди: ${node.nodeType} Назва ноди: + ${node.nodeName}`);
}

let testParagraph = document.querySelector('#testParagraph');
testParagraph.innerHTML = '<p>This is a paragraph</p>';


let mainHeader = document.querySelector('.main-header').children;
console.log(mainHeader);

for(let elem of mainHeader){
    elem.classList.add('nav-item');
}

let sectionTitle = document.querySelectorAll('.section-title');
for(let elem of sectionTitle){
    elem.classList.remove('section-title');
}
